# EDU Verification Bot
## Purpose

This is a simple bot that has a database and helps verify an email address of any user. It responds to a whisper and expects either an email address or a number.

If an email is provided an email is generated with a securely generated 6 digit number for that email.

If a number is provided it is checked against the database of verification numbers and if found it adds that user to the user table and links them with the allowed domains from the districts table.

The bot then creates and adds roles as appropriate.

## License
I have chosen teh GNU v3 license so feel free to make a copy and modify the code in accordance with that license. This means (amongst other rules) that you need to make your code public and use the same license.

Also I provide no warranty for this product.

## Installation
Feel free to fork and use on your own server and bot. The bot works off of whispers only and not !commands.

You'll need to create a .env file in the root, database, and mailer folders.

root .env should contain:
    
    \# .env
    DISCORD_TOKEN=
    SERVER_ID=

The database folder should contain a .env file with:

    \# .env
    db_engine=mysql+pymysql
    db_username=
    db_password=
    db_server=
    db_port=
    db_database=

Feel free to use a different db_engine per sqlalchemy's instructions

The mailer folder should contain a .env file with:

    \# .env
    smtp_server=
    port=
    username=
    password=
    sender_email=

I recommend using a virtual environment and once created running something like:

    source venv/bin/activate

Required packages include:

    pip3 install alembic discord.py PyMySQL email_validator sqlalchemy python-dotenv

Then run the migration to set up your database:

    alembic upgrade head

Finally, start the script:
    
    Then run python3 edu_verification.py &
