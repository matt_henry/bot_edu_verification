"""
Used to perform email related tasks
"""
import smtplib
import ssl
import os

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from dotenv import load_dotenv

load_dotenv()

def send_verification_email(recipient, code, expiration):
    """
    Sends a verification email to a recipient with their code and expiration date
    """
    smtp_server = os.getenv('smtp_server')
    port = os.getenv('port')
    username = os.getenv('username')
    password = os.getenv('password')
    sender_email = os.getenv('sender_email')

    message = MIMEMultipart("alternative")
    message["Subject"] = "Discord Verification"
    message["From"] = sender_email
    message["To"] = recipient
    # write the plain text part
    text = """\
    Hi,
    Your verification code is: {0}

    Please reply to the verification bot with your code. This code will expire on {1}
    """.format(code, expiration)
    # write the HTML part
    html = """\
    <html>
    <body>
        <p>Hi,<br>
        Your verification code is:</p>
        <p>{0}</p>
        <p> Please reply to the verification bot with your code. This code will expire on {1}</p>
    </body>
    </html>
    """.format(code, expiration)
    # convert both parts to MIMEText objects and add them to the MIMEMultipart message
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    message.attach(part1)
    message.attach(part2)
    server = smtplib.SMTP_SSL(smtp_server, port)
    server.login(username, password)
    server.sendmail(
        sender_email, recipient, message.as_string()
    )

    # send your email
#    with smtplib.SMTP(smtp_server, port) as server:
#        server.login(username, password)
##        server.sendmail(
 #           sender_email, recipient, message.as_string()
 #       )
