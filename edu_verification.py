# edu_verification
import os
import discord
import datetime
import secrets

from discord.ext import commands
from dotenv import load_dotenv
from email_validator import validate_email, EmailNotValidError
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy import create_engine, text
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.orm.exc import NoResultFound

from database.db import get_url
from mailer.mailer import send_verification_email
from database.schema.dbmodel import Base, Verification, District, User

# Enable Member Intents
intents = discord.Intents().default()
intents.members = True

# Load environment Information
load_dotenv()

# Retrieve Token
TOKEN = os.getenv('DISCORD_TOKEN')

# Connect to database
engine = create_engine(get_url())
Session = sessionmaker(bind=engine)
session = Session()

# Initialize random number generator
secretsGenerator = secrets.SystemRandom()

# Set Bot Command Prefix    
bot = commands.Bot(command_prefix='', intents=intents)

@bot.event
async def on_message(message):
    # Remove white spaces etc

    message.content = message.content.strip()
    if not hasattr(message.channel, 'me') or message.author.id == message.channel.me.id:
        return
    if isinstance(message.channel, discord.channel.DMChannel ):
        if(message.content.find('@') != -1): # Contains @ symbol? Must be an email
            await process_email(message)
        elif(message.content.isnumeric()): # Contains only numbers? Must be a verification code
            await process_verification(message)
        else:
            await message.channel.send('I am the verification bot. If you need to verify your account just let me know your email address and I will email you a code.')

def get_expiration(hours = 1):
    return datetime.datetime.now() + datetime.timedelta(hours = hours)

def get_code():
    return secretsGenerator.randint(100000,999999)

async def process_verification(message):
    session = Session()
    try:
        verification = session.query(Verification).filter(Verification.user_id == message.author.id, Verification.code ==  message.content).one()
    except NoResultFound as e:
        await message.channel.send('No verification code found with that number. Please check your email and try again.')
        return

    try:
        nickname = (verification.email.split("@")[0]).replace('.',' ').title()
        email_domain = verification.email.split("@")[1]
        district = session.query(District).filter(District.domain == email_domain).one()
    except NoResultFound as e:
        await message.channel.send('The domain "{0}" associated with this verification code is no longer valid.'.format(email_domain))
        return

    if verification.expiration <= datetime.datetime.now():
        await message.channel.send('This code has expired. Please provide your email address again to generate a new one.')
        return
    db_user = User( username=verification.username, user_id=verification.user_id, email=verification.email, creation_date=datetime.datetime.now(), district_id=district.id )
    session.add(db_user)
    session.delete(verification)
    session.commit()
    await message.channel.send('Your domain has been verified. Please wait a moment...')
    try:
        guild = discord.utils.get(bot.guilds, id=int(os.getenv('SERVER_ID')))
        member = await guild.fetch_member(message.author.id)
        roles = []
        roles.append( await get_role(guild, 'Verified'))
        roles.append( await get_role(guild, db_user.district.name))
        roles.append( await get_role(guild, 'Region ' + db_user.district.region))
        await member.add_roles(*roles)
        await message.channel.send('Done! You have been assigned the appropriate roles.')
    except Exception as e:
        await message.channel.send('Sorry, something went wrong.')

    try:
        await member.edit(nick=nickname)
        await message.channel.send('Your nickname has been set to {0}'.format(nickname))
    except Exception as e:
        await message.channel.send('Failed to set your nickname. Speak to an admin to get this fixed.')

async def get_role(guild, name):
    role = discord.utils.get(guild.roles, name=name)
    if role:
        return role
    return await guild.create_role(name=name)

async def process_email(message):
    session = Session()
    async with message.channel.typing():
        email = message.content.strip()
        try:
            valid = validate_email(email)
        except EmailNotValidError as e:
            await message.channel.send('{0} was not a valid email address'.format(email) )
            return
        
        found_user = session.query(User).filter(User.email == valid.email).first()
        if found_user:
            await message.channel.send('This email is already verified.')
            return

        try:
            session.query(District).filter(District.domain == valid.domain).one() #pylint: disable=no-member
        except NoResultFound as e:
            await message.channel.send( 'I am sorry but the domain of {0} is not recognized as a valid KY Education domain. Please provide a valid KY Education email address.'.format(valid.domain))
            return

        try:
            # Generate Verification Code
            code = get_code()

            # Generate Expiration Date
            expiration = get_expiration()
            
            # If user has a verification code(s) already delete them
            existing_verifications = session.query(Verification).filter(Verification.user_id == message.author.id).all()
            if(existing_verifications):
                for verification in existing_verifications:
                    session.delete(verification)
                session.commit()
            # Add verification code to database
            new_verification = Verification(username=message.author.name, user_id=message.author.id ,email=valid.email,code=code,expiration=expiration)
            session.add( new_verification )
            session.commit()
            send_verification_email(valid.email, code, expiration)
            await message.channel.send( 'Thank you for providing a valid email address. Please check your email and reply with your verification code.' )
            await message.channel.send( 'The code will expire in 1 hour.' )
        except Exception as e:
            await message.channel.send( 'I am sorry. An error has ocurred. Please try again later.' )

@bot.event
async def on_member_remove(member):
    session = Session()
    db_user = session.query(User).filter(User.user_id == member.id).delete()
    session.commit()

@bot.event
async def on_member_join(member):
    await member.send('I am the verification bot. If you need to verify your account just let me know your email address and I will email you a code.')

@bot.command()
async def ping(ctx):
    await ctx.send('pong')

bot.run(TOKEN)
