import os
import urllib
from dotenv import load_dotenv

#from lib import db
load_dotenv()

def get_url() -> str:
    return '{}://{}:{}@{}:{}/{}'.format(
        os.getenv('db_engine'),
        os.getenv('db_username'),
        urllib.parse.quote_plus( os.getenv('db_password') ),
        os.getenv('db_server'),
        os.getenv('db_port'),
        os.getenv('db_database')
    )