from sqlalchemy import Column, DateTime, String, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class Verification(Base):
    __tablename__ = 'verification'
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    user_id = Column(String(255))
    email = Column(String(255))
    code = Column(String(255))
    expiration = Column(DateTime)

class District(Base):
    __tablename__ = 'district'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    region = Column(String(255))
    domain = Column(String(255))
    users = relationship("User", back_populates="district")

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    user_id = Column(String(255))
    email = Column(String(255))
    district_id = Column(Integer, ForeignKey('district.id'))
    district = relationship( "District", back_populates = "users" )
    creation_date = Column(DateTime)
